# Logflix

[![Netlify Status](https://api.netlify.com/api/v1/badges/da2ad51b-2262-4fe1-b8d1-45e975f8711f/deploy-status)](https://app.netlify.com/sites/logflix/deploys)

Logflix is a minimal web app for a collection of movies and tv series.

## Features

- No framework dependency
- HTML5 navigation
- Offline support
- Mobile optimized

## Dependencies

- [router.js](https://github.com/Polymer/pwa-helpers#routerjs)

## Development Requirements

- [Node.js](https://nodejs.org)

## Installation

Use any Node.js package manager to install project dependencies.

```bash
$ npm install
```

## Developing

Start the local server for developmenmt.

```bash
$ npm start
```

## Build

Generate production-ready assets for deploy.

```bash
$ npm run build
```

## Credits

The project is powered by [The Movie Database](https://www.themoviedb.org) API.

## License

[MIT](https://choosealicense.com/licenses/mit/)
