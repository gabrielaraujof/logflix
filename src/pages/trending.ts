import {
  LitElement,
  customElement,
  html,
  css,
  property,
  TemplateResult,
  CSSResult,
} from 'lit-element';

import { ApiService } from '../core/api';
import '../components/content-card';

const genreMapping = (movieGenreMap, tvGenreMap) => mediaType => (genreId): string => {
  switch (mediaType) {
    case 'tv':
      return tvGenreMap[genreId];
    case 'movie':
    default:
      return movieGenreMap[genreId];
  }
};

@customElement('lf-trending')
export default class Trending extends LitElement {
  service;

  @property({ type: Array })
  contents = [];

  static get styles(): CSSResult {
    return css`
      section {
        margin: 0.8rem;
      }

      section > h1 {
        font-size: 1.6rem;
      }

      section > h1,
      section > ul {
        margin: 1.6rem 0;
      }

      section > ul {
        display: grid;
        grid-gap: 0.8rem;
        margin: 0;
        padding: 0;
        list-style: none;
      }

      @media screen and (max-width: 599px) {
        section > ul {
          grid-template-columns: repeat(2, 1fr);
        }
      }

      @media screen and (min-width: 600px) {
        section > ul {
          grid-template-columns: repeat(4, 1fr);
        }
      }

      @media screen and (min-width: 900px) {
        section > ul {
          grid-template-columns: repeat(6, 1fr);
        }
      }

      @media screen and (min-width: 1200px) {
        section > ul {
          grid-template-columns: repeat(8, 1fr);
        }
      }
    `;
  }

  constructor() {
    super();
    this.service = new ApiService();
  }

  connectedCallback(): void {
    super.connectedCallback();
    Promise.all([
      this.service.fetchTrending(),
      this.service.fetchMovieGenres(),
      this.service.fetchTVGenres(),
    ]).then(([{ results }, movieGenreMap, tvGenreMap]) => {
      const toGenderName = genreMapping(movieGenreMap, tvGenreMap);
      this.contents = results.map(item => {
        const genres = item.genre_ids.map(toGenderName(item.media_type));
        return { ...item, genres };
      });
    });
  }

  render(): TemplateResult {
    return html`
      <section>
        <h1>Trending</h1>
        <ul>
          ${this.contents.map(
            (content: any) => html`
              <li>
                <lf-content-card
                  .id="${content.id}"
                  .title="${content.title || content.name}"
                  .type="${content.media_type}"
                  .poster="${content.poster_path}"
                  .release="${content.release_date || content.first_air_date}"
                  .genres="${content.genres}"
                ></lf-content-card>
              </li>
            `,
          )}
        </ul>
      </section>
    `;
  }
}
