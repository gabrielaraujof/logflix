import { LitElement, html, customElement, css, CSSResult, TemplateResult } from 'lit-element';

@customElement('lf-offline')
export default class Offline extends LitElement {
  static get styles(): CSSResult {
    return css`
      section {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        padding: var(--gap-default, 24px);
        height: var(--full-height, 100vh);
        text-align: center;
      }

      svg {
        margin: 1.6em;
        width: 10em;
        height: 10em;
        fill: var(--grey, #666);
      }

      h1,
      h2 {
        margin: 0.8em 0;
      }

      h2 {
        font-size: 1.6em;
      }
    `;
  }

  render(): TemplateResult {
    return html`
      <section>
        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 612 612">
          <path
            d="M 494.7,229.5 C 476.849,142.8 400.349,76.5 306,76.5 c -38.25,0 -73.95,10.2 -102,30.6 l 38.25,38.25 C 260.1,132.6 283.05,127.5 306,127.5 c 76.5,0 140.25,63.75 140.25,140.25 v 12.75 h 38.25 c 43.35,0 76.5,33.15 76.5,76.5 0,28.05 -15.3,53.55 -40.8,66.3 l 38.25,38.25 C 591.6,438.6 612,400.35 612,357 612,290.7 558.45,234.6 494.7,229.5 Z M 76.5,109.65 147.9,178.5 C 66.3,183.6 0,249.9 0,331.5 c 0,84.15 68.85,153 153,153 h 298.35 l 51,51 L 535.5,502.35 109.65,76.5 Z m 119.85,119.85 204,204 H 153 c -56.1,0 -102,-45.9 -102,-102 0,-56.1 45.9,-102 102,-102 z"
          />
        </svg>
        <h1>Unavailable while offline</h1>
        <h2>You must reconnect to see this page.</h2>
      </section>
    `;
  }
}
