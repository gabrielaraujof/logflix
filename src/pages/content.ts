import {
  LitElement,
  customElement,
  html,
  css,
  property,
  CSSResult,
  TemplateResult,
} from 'lit-element';
import { styleMap } from 'lit-html/directives/style-map';
import { classMap } from 'lit-html/directives/class-map';

import { backdropSources } from '../core/helper';
import { ApiService } from '../core/api';
import '../pages/offline';

@customElement('lf-content')
export default class Content extends LitElement {
  service: ApiService;
  content;
  networkError = false;

  @property({ type: Number }) id;
  @property({ type: String }) media;

  get releaseYear(): string {
    if (this.content) {
      const releaseDate = this.content.release_date || this.content.first_air_date;
      const [releaseYear] = releaseDate.split('-');
      return releaseYear;
    }
    return '';
  }

  static get styles(): CSSResult {
    return css`
      section {
        margin: 1.6rem;
      }

      section h1 {
        margin: 2.4rem 0;
        font-size: 3.6rem;
      }

      section h2,
      section h3 {
        margin: 0.8rem 0;
        font-size: 2.4rem;
      }

      section .rating {
        display: flex;
      }

      section .rating-average,
      section .rating-count {
        display: inline-block;
      }

      section .rating-average.rating-average--high {
        color: #098219;
      }

      section .rating-average.rating-average--low {
        color: #c21010;
      }

      section ul {
        margin: 0;
        padding: 0;
        list-style: none;
      }

      section ul li {
        display: inline-block;
        margin-top: 0.8rem;
        margin-right: 0.8rem;
        padding: 0.8rem;
        border-radius: 3.2rem;
        border: 1px solid #ddd;
        line-height: 1;
      }

      section figure {
        margin: 2.4rem 0;
      }

      section figure img {
        margin-left: -1.6rem;
        width: 100vw;
      }

      section p {
        line-height: 1.5;
        text-align: justify;
      }

      section .rating-stars {
        margin: 0 0.8rem;
        width: 12rem;
      }

      section .rating-stars,
      section .rating-stars > span {
        position: relative;
        display: inline-block;
        height: 2.4rem;
      }

      section .rating-stars::before {
        right: 0;
        background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2ZXJzaW9uPSIxLjAiIHg9IjBweCIgeT0iMHB4IiB2aWV3Qm94PSIwIDAgNTAgNTAiIGZpbGw9IiM3ZjhjOGQiID4gICAgPHBhdGggZD0iTSAyNSAyLjI3MzQzNzUgTCAxOC42MDkzNzUgMTguNDgwNDY5IEwgMC44MTA1NDY4OCAxOS40MTc5NjkgTCAxNC42NDg0MzggMzAuNDEyMTA5IEwgMTAuMDcwMzEyIDQ3LjIyMjY1NiBMIDI1IDM3Ljc3MTQ4NCBMIDM5LjkyOTY4OCA0Ny4yMjI2NTYgTCAzNS4zNTE1NjIgMzAuNDEyMTA5IEwgNDkuMTg5NDUzIDE5LjQxNzk2OSBMIDMxLjM5MDYyNSAxOC40ODA0NjkgTCAyNSAyLjI3MzQzNzUgeiI+PC9wYXRoPjwvc3ZnPg==);
      }

      section .rating-stars > span::before {
        width: 100%;
        background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2ZXJzaW9uPSIxLjAiIHg9IjBweCIgeT0iMHB4IiB2aWV3Qm94PSIwIDAgNTAgNTAiIGZpbGw9IiNmMWM0MGYiID4gICAgPHBhdGggZD0iTSAyNSAyLjI3MzQzNzUgTCAxOC42MDkzNzUgMTguNDgwNDY5IEwgMC44MTA1NDY4OCAxOS40MTc5NjkgTCAxNC42NDg0MzggMzAuNDEyMTA5IEwgMTAuMDcwMzEyIDQ3LjIyMjY1NiBMIDI1IDM3Ljc3MTQ4NCBMIDM5LjkyOTY4OCA0Ny4yMjI2NTYgTCAzNS4zNTE1NjIgMzAuNDEyMTA5IEwgNDkuMTg5NDUzIDE5LjQxNzk2OSBMIDMxLjM5MDYyNSAxOC40ODA0NjkgTCAyNSAyLjI3MzQzNzUgeiI+PC9wYXRoPjwvc3ZnPg==);
      }

      section .rating-stars::before,
      section .rating-stars > span::before {
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        background-repeat: repeat-x;
        background-size: 2.4rem;
        content: '';
      }
    `;
  }

  constructor() {
    super();
    this.service = new ApiService();
  }

  updated(): void {
    if (this.id && this.media && !this.content && !this.networkError) {
      this.service
        .fetchContent(this.id, this.media)
        .then(content => {
          this.content = content;
        })
        .catch(() => {
          this.networkError = true;
        })
        .finally(() => this.requestUpdate());
    }
  }

  render(): TemplateResult {
    if (this.networkError) {
      return html`
        <lf-offline></lf-offline>
      `;
    }
    if (!this.content) {
      return html``;
    }
    const headline = this.content.title || this.content.name;
    const [srcset, srcdefault] = backdropSources(this.content.backdrop_path);
    return html`
      <section>
        <header>
          <h1>${headline}</h1>
          <h2 class="rating">
            <span
              class="rating-average"
              class="${classMap({
                'rating-average--high': this.content.vote_average >= 8,
                'rating-average--low': this.content.vote_average < 6,
              })}"
              >${this.content.vote_average.toFixed(1)}</span
            >
            <span class="rating-stars">
              <span
                style=${styleMap({
                  width: `${this.content.vote_average * 10}%`,
                })}
              ></span>
            </span>
            <span class="rating-count">(${this.content.vote_count})</span>
          </h2>
          <h2 class="release-date">${this.releaseYear}</h2>
          <ul>
            ${this.content.genres.map(
              ({ name }) =>
                html`
                  <li>${name}</li>
                `,
            )}
          </ul>
        </header>
        <figure>
          <img
            crossorigin="anonymous"
            alt="${headline}"
            .src="${srcdefault}"
            .srcset="${srcset.map(src => src.join(' ')).join(', ')}"
          />
        </figure>
        <p>${this.content.overview}</p>
      </section>
    `;
  }
}
