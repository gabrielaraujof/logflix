import {
  LitElement,
  customElement,
  html,
  css,
  property,
  TemplateResult,
  CSSResult,
} from 'lit-element';

import { posterSources } from '../core/helper';

@customElement('lf-content-card')
export class ContentCard extends LitElement {
  @property({ type: Number }) id;

  @property({ type: String }) title;

  @property({ type: String }) type;

  @property({ type: String }) poster;

  @property({ type: String }) release;

  @property({ type: Array }) genres;

  get releaseYear(): string {
    const [year] = this.release.split('-');
    return year;
  }

  static get styles(): CSSResult {
    return css`
      :host {
        display: block;
        margin: 0;
        border-radius: 1.2rem;
        box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.16), 0 0 0 1px rgba(0, 0, 0, 0.08);
        height: 100%;
        overflow: hidden;
      }

      a {
        text-decoration: none;
      }

      a,
      a:hover,
      a:active,
      a:visited {
        color: inherit;
      }

      h1,
      h2,
      h3 {
        margin: 0.8rem 0;
      }

      h1 {
        font-size: 1.6rem;
        font-weight: 700;
      }

      h2,
      h3 {
        font-size: 1.2rem;
      }

      figure {
        margin: 0;
      }

      img {
        width: 100%;
      }

      figure figcaption {
        margin: 1.6rem 0.8rem 0.8rem;
      }
    `;
  }

  render(): TemplateResult {
    const [srcset, srcdefault] = posterSources(this.poster);
    return html`
      <a href="${this.type}/${this.id}">
        <figure>
          <img
            alt="${this.title} poster"
            .srcset="${srcset.map(src => src.join(' ')).join(', ')}"
            .src="${srcdefault}"
            crossorigin="anonymous"
            sizes="(min-width: 600px) 25vw,
               (min-width: 900px) 16.7vw,
               (min-width: 1200px) 12.5vw,
               50vw"
          />
          <figcaption>
            <h1>${this.title}</h1>
            <h2>${this.releaseYear}</h2>
            <h3>${this.genres.join(' / ')}</h3>
          </figcaption>
        </figure>
      </a>
    `;
  }
}

export default ContentCard;
