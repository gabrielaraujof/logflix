import { clientsClaim } from 'workbox-core/clientsClaim';
import { setCacheNameDetails } from 'workbox-core/setCacheNameDetails';
import { registerRoute } from 'workbox-routing/registerRoute';
import { CacheFirst } from 'workbox-strategies/CacheFirst';
import { StaleWhileRevalidate } from 'workbox-strategies/StaleWhileRevalidate';
import { ExpirationPlugin } from 'workbox-expiration/ExpirationPlugin';
import { CacheableResponsePlugin } from 'workbox-cacheable-response/CacheableResponsePlugin';
import { precacheAndRoute } from 'workbox-precaching/precacheAndRoute';

clientsClaim();

setCacheNameDetails({
  prefix: 'logflix',
  precache: 'precache',
  runtime: 'runtime',
});

registerRoute(
  /.+\/image\.tmdb\.org\/t\/p/,
  new CacheFirst({
    cacheName: 'tmdb-images',
    plugins: [
      new ExpirationPlugin({
        maxEntries: 60,
        maxAgeSeconds: 30 * 24 * 60 * 60, // 30 Days
      }),
      new CacheableResponsePlugin({
        statuses: [0, 200],
      }),
    ],
  }),
);

registerRoute(
  /.+\/api\.themoviedb\.org\/3\/genre/,
  new CacheFirst({
    cacheName: 'tmdb-genres',
    plugins: [
      new ExpirationPlugin({
        maxAgeSeconds: 30 * 24 * 60 * 60, // 30 Days
      }),
      new CacheableResponsePlugin({
        statuses: [0, 200],
      }),
    ],
  }),
);

registerRoute(
  /.+\/api\.themoviedb\.org\/3\/(trending|movie|tv)/,
  new StaleWhileRevalidate({
    cacheName: 'tmdb',
    plugins: [
      new ExpirationPlugin({
        maxEntries: 30,
        maxAgeSeconds: 15 * 24 * 60 * 60, // 15 Days
      }),
      new CacheableResponsePlugin({
        statuses: [0, 200],
      }),
    ],
  }),
);

precacheAndRoute(self.__WB_MANIFEST || []);
