import { LitElement, html, customElement, css, CSSResult, TemplateResult } from 'lit-element';

import './pages/trending.ts';
import './pages/content.ts';

import './styles.css';

@customElement('lf-app')
export default class App extends LitElement {
  static get styles(): CSSResult {
    return css`
      h2,
      h3 {
        color: #666;
      }
    `;
  }

  render(): TemplateResult {
    return html`
      <lit-route path="/" component="lf-trending"> </lit-route>
      <lit-route path="/:media/:id" component="lf-content"> </lit-route>
      <lit-route component="lf-not-found"> </lit-route>
    `;
  }
}
