function toGenreMap(genres): any {
  return genres.reduce((prev, { id, name }) => Object.assign(prev, { [id]: name }), {});
}

const API_URL = 'https://api.themoviedb.org/3';
const API_TOKEN = '04ff5f0ef8c35b66fb5558f4ba861390';

export class ApiService {
  baseUrl;

  apiToken;

  constructor() {
    this.baseUrl = API_URL;
    this.apiToken = API_TOKEN;
  }

  builUrl(path, params = {}): string {
    const paramList = Object.keys(params)
      .map(key => `${key}=${params[key]}`)
      .concat([`api_key=${this.apiToken}`]);
    return `${this.baseUrl}/${path}?${paramList.join('&')}`;
  }

  fetchMovieGenres(): Promise<any> {
    const url = this.builUrl('genre/movie/list', { language: 'en-US' });
    return fetch(url)
      .then(res => res.json())
      .then(({ genres }) => toGenreMap(genres));
  }

  fetchTVGenres(): Promise<any> {
    const url = this.builUrl('genre/tv/list', { language: 'en-US' });
    return fetch(url)
      .then(res => res.json())
      .then(({ genres }) => toGenreMap(genres));
  }

  fetchTrending(): Promise<any> {
    const url = this.builUrl('trending/all/week');
    return fetch(url).then(res => res.json());
  }

  fetchMovie(id): Promise<any> {
    const url = this.builUrl(`movie/${id}`, { language: 'en-US' });
    return fetch(url).then(res => res.json());
  }

  fetchTV(id): Promise<any> {
    const url = this.builUrl(`tv/${id}`, { language: 'en-US' });
    return fetch(url).then(res => res.json());
  }

  fetchContent(id, mediaType): Promise<any> {
    switch (mediaType) {
      case 'tv':
        return this.fetchTV(id);
      case 'movie':
      default:
        return this.fetchMovie(id);
    }
  }
}
