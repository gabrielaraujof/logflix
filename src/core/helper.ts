const BASE_IMAGE_URL = 'https://image.tmdb.org/t/p';

const POSTER_SIZES = [92, 154, 185, 342, 500, 780];
const BACKDROP_SIZES = [300, 780, 1280];

const toImageUrl = filePath => size => [`${BASE_IMAGE_URL}/w${size}${filePath}`, `${size}w`];

function sources(filePath, sizes): string[][] {
  return sizes.map(toImageUrl(filePath));
}

export function backdropSources(filePath): [string[][], string] {
  const srcset = sources(filePath, BACKDROP_SIZES);
  return [srcset, srcset[1][0]];
}

export function posterSources(filePath): [string[][], string] {
  const srcset = sources(filePath, POSTER_SIZES);
  return [srcset, srcset[3][0]];
}
