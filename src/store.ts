import { createStore, combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { lazyReducerEnhancer } from 'pwa-helpers';

const composeEnhancers = composeWithDevTools({});

const store = createStore(
  (state: any): any => state,
  composeEnhancers(lazyReducerEnhancer(combineReducers)),
);

export default store;
