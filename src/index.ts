import { connectRouter } from 'lit-redux-router';

import store from './store';
import './app.ts';

connectRouter(store);

if ('serviceWorker' in navigator) {
  window.addEventListener('load', () => {
    navigator.serviceWorker.register('/sw.js');
  });
}
