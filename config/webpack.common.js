const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const TerserJSPlugin = require('terser-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const WorkboxPlugin = require('workbox-webpack-plugin');

module.exports = {
  entry: {
    app: './src/index.ts',
  },
  output: {
    path: path.resolve(__dirname, '..', 'dist'),
    filename: '[name].[chunkhash:6].js',
    chunkFilename: '[name].[chunkhash:6].chunk.js',
    publicPath: '/',
  },
  devServer: {
    historyApiFallback: true
  },
  module: {
    rules: [
      { test: /\.ts$/, use: 'ts-loader', exclude: /node_modules/  },
      { test: /\.(jpe?g|png|gif|ico)$/i, loader: 'file?name=[name].[ext]' },
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          { loader: 'css-loader', options: { importLoaders: 1 } },
          {
            loader: 'postcss-loader',
            options: {
              plugins: [require('autoprefixer')],
            },
          },
        ],
      },
    ],
  },
  resolve: {
    extensions: ['.ts', '.js' ],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: 'src/index.html',
      favicon: 'src/favicon.ico',
      inject: true,
      minify: {
        collapseWhitespace: true,
        removeComments: true,
        removeRedundantAttributes: true,
        removeScriptTypeAttributes: true,
        removeStyleLinkTypeAttributes: true,
        useShortDoctype: true,
      },
    }),
    new CopyWebpackPlugin([{ from: 'public/' }]),
    new MiniCssExtractPlugin({
      filename: '[name].[chunkhash:6].css',
      chunkFilename: '[id].[chunkhash:6].chunk.css',
    }),
    new WorkboxPlugin.InjectManifest({
      swSrc: path.resolve(__dirname, '..', 'src', 'sw.js'),
      swDest: path.resolve(__dirname, '..', 'dist', 'sw.js'),
      exclude: [
        'manifest.json',
        '_redirects',
        'robots.txt',
        /\.map$/,
        /(?:icon|splash).*\.png$/,
      ],
    }),
  ],
  optimization: {
    minimizer: [
      new TerserJSPlugin({
        test: /\.m?js$/,
      }),
      new OptimizeCSSAssetsPlugin({}),
    ],
  },
};
